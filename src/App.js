import "./styles/css/main.css";
import React, { useState } from "react";
import Message from "./components/Message/Message";
import MainContainer from "./components/MainContainer/MainContainer";
import Footer from "./components/Footer/Footer";

function App() {
	const [color, setColor] = useState(getColorGradient());
	const [wasClicked, setWasClicked] = useState(false);

	function handleClick() {
		setColor(getColorGradient());
	}

	function getRandomNumber() {
		return Math.round(Math.random() * 255);
	}

	function getColor() {
		let number1 = getRandomNumber();
		let number2 = getRandomNumber();
		let number3 = getRandomNumber();

		return `rgb(${number1}, ${number2}, ${number3})`;
	}

	function getColorGradient() {
		return `linear-gradient(180deg, ${getColor()}, ${getColor()})`;
	}

	function handleCopyClipboard(condition) {
		setWasClicked(condition);
	}

	return (
		<div className='app-container' style={{ backgroundImage: color }}>
			<Message handleCopyClipboard={handleCopyClipboard} wasClicked={wasClicked}></Message>
			<MainContainer
				color={color}
				handleClick={handleClick}
				handleCopyClipboard={handleCopyClipboard}></MainContainer>
			<Footer></Footer>
		</div>
	);
}

export default App;
