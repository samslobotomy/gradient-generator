import React from 'react'

function Footer() {
  return (
    <footer className="footer">
      <p>© 2021 — coded by <a href="https://bitbucket.org/samslobotomy/gradient-generator" target="_blank" rel="noreferrer">samanta carvalho </a>with react.js</p>
    </footer>
  )
}

export default Footer;
