import React from "react";
import GenerateContainer from "../GenerateContainer/GenerateContainer.js";
import ColorBlob from "../ColorBlob/ColorBlob.js";

function MainContainer(props) {
	return (
		<main className='main-container'>
			<div className='title'>
				<h1>Color Gradient Generator</h1>
			</div>
			<GenerateContainer handleClick={props.handleClick}/>
			<ColorBlob color={props.color} handleCopyClipboard={props.handleCopyClipboard}/>
		</main>
	);
}

export default MainContainer;
