import React from "react";

function ColorBlob(props) {

	function copyToClipboard() {
		navigator.clipboard.writeText(props.color).then(
			function () {
				console.log("success");
			},
			function () {
				console.log("fail");
			}
		);
	}

	return (
		<div className='color-blob-container'>
			<div
				className='color-blob'
				style={{ backgroundImage: props.color }}></div>
			<div className='flex' style={{ marginTop: "60px" }}>
				<p>copy gradient</p>
				<div className='copy-icon' onClick={()=>{
					copyToClipboard()
					props.handleCopyClipboard(true)
				}} >
					<span className='tooltip'>copy to clipboard</span>
				</div>
			</div>
		</div>
	);
}

export default ColorBlob;
