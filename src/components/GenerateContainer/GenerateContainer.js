import React from "react";

function GenerateContainer(props) {
	return (
		<div className='generate-container'>
			<p>CSS color gradients for cool backgrounds and UI elements</p>
			<p>Generate to see what happens!</p>
			<div
				className='btn'
				onClick={function () {
					props.handleClick();
				}}>
				<span className='btn-text'>Generate</span>
			</div>
		</div>
	);
}

export default GenerateContainer;
