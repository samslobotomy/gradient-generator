import React, { useEffect } from "react";

function Message(props) {
	useEffect(() => {
		const div = document.querySelector(".message-info");

		if (props.wasClicked) {
			div.style.opacity = 1;
			setTimeout(() => {
				props.handleCopyClipboard(false);
			}, 1500);
		} else {
			div.style.opacity = 0;
		}
	});

	return (
		<div className='message-info'>
			<p>gradient copied to your clipboard</p>
		</div>
	);
}

export default Message;
